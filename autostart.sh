#! /usr/bin/env sh

LOC=$(readlink -f "$0")
DIR=$(dirname "$LOC")

feh --bg-fill $DIR/wall.png &
picom &
lxsession &

pkill slstatus; slstatus & 
